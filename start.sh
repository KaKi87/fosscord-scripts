#!/usr/bin/env bash
export $(cat .env | grep -v "^#" | xargs)
cd ./_bundle
npm run start:bundle
